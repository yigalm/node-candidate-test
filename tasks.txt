Issue #1
Our infra layer developers created a new fileUtil.js with the following function => logFileContent(fullName)
the intention of this function is logging to the console the content of a given file.

Run the application (main.js) and find out what went wrong. 
suggest two different ways to fix it.

Enhancement #1
Your file path is currently a hard coded one. not a really good practice.
it will be a better approch to externalize it to a config file.
use the global.js file (can be found under the config folder) and move the hard coded file details into that file
note: keep both name & path as a seperate fields

use the global.js before calling the logFileContent function.

Enhancement #2
Enhance the config/global.js file to enable merging with a custom config file.
the new config file can be found under config/custom.js
the new custom value is - { name: '.custom-data.txt' }
==>
global.js
module.exports = {
    path: "./files",
    name: 'global-data.txt',
}
custom.js
module.exports = {
    name: 'custom-data.txt',
}

main.js
let newMergedSetting = merge...()

use your new merged values and call the logFileContent function.
Tip: you can use the lodash npm package to make your life more easier

Enhancement #3 
New requirement just arrived - we need to search for a specific file within a folder (by a given fullPath)
and BREAK if the file was found
we already have a function that can search by path: fileUtil.logDirectory(path);
enhance it to stop searching (break) when a file was found

Enhancement #4
The content of the files/global-data.txt is very important to us.
write a small client.js that can POST the file content in a json format to our server/server.js. 
note - you can read the file content and send it as part of the body message

from the server side - just console.log the content that was sended

Enhancement #5
Now that you have the content, save it in the same path that it was reading from the beginning
but with the suffix _copy