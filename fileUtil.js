'use strict'

var fs = require('fs');

async function logFileContent(filePath){
    let content = readFileContent(filePath);
    console.log(`file content: ${content}`);
}

function readFileContent(filePath){
    fs.readFile(filePath, 'utf-8', (err, content) => {
        return content;
    });
}

function logDirectory(path){
    let files = getFiles(path);
    files.forEach(file => {
        console.log(file);
    }); 
}

function getFiles(path) {
    return fs.readdirSync(path).filter(function (file) {
      return fs.statSync(path + '/' + file);//.isDirectory();
    });
}

module.exports = {
    logFileContent: logFileContent,
    logDirectory : logDirectory
}
