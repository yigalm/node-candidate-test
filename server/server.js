
var express = require('express');

var app = express();

app.get('/', function(req, res) {
    res.send(`hello!`);
});

app.post('/file', function(req, res) {
    res.send('TODO: file saved!')
});

var server = app.listen(8088, function(){
    var port = server.address().port;

    console.log(`server is up! listing on http://localhost:${port}`)
});