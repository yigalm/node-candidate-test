
'use strict'

var logFile = require('./fileUtil.js');
var rootConfig = require('./config/rootConfig');

let path = rootConfig.path;
let name = rootConfig.name;

let fileName = `${path}/${name}`;

logFile.logFileContentWithPromise(fileName);
