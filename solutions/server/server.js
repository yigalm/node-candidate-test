
var express = require('express');
var bodyParser = require('body-parser');
var fs = require('fs');
var myConfig = require('../solutions/config/myConfig');
var rootConfigExt = require('../solutions/config/rootConfigExtended');

let customConfig = rootConfigExt(myConfig);

var app = express();
app.use(bodyParser.urlencoded({ extended: false}));
app.use(bodyParser.json());

app.get('/file', function(req, res) {
    res.send('please use the /file/{id} endpoint to retrieve a file content');
});

app.post('/file', function(req, res) {

    let path = customConfig.path;
    let name = customConfig.name;
    if (req.body.name){
        name = req.body.name;
    }

    let fileName = `${path}\\${name}`;

    fs.writeFile(fileName, JSON.stringify(req.body), (err) => {
        if (err) {
            console.log(err);
        }
        res.send(req.body);
    })
})

app.get('/file/:id', function(req, res) {
    if (!req.params.id){
        res.send('please send an id');
        return;
    }
    res.send(`${req.params.id}`);
});

app.get('/', function(req, res) {
    res.send(`hello!`);
});

var server = app.listen(8088, function(){
    var port = server.address().port;

    console.log(`server is up! listing on http://localhost:${port}`)
});