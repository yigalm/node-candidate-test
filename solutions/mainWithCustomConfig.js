'use strict'

var rootConfigExt = require('./config/rootConfigExtended');
var myConfig = require('./config/myConfig');
var logFile = require('./fileUtil');

let customConfig = rootConfigExt(myConfig);

let path = customConfig.path;
let name = customConfig.name;

let fileName = `${path}\\${name}`;

logFile.logFileContentWithPromise(fileName);
