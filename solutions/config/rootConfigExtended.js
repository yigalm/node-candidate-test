'use strict'

var _ = require('lodash');

module.exports = (customConfig) => {
    const config = _.defaults(customConfig, {
        path: "c:\\fullStackCandidateTest\\candidateTestingFiles",
        name: 'content.txt',
    });

    return config;
};